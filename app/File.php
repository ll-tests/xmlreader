<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = "files";
    protected $fillable = ["filename", "original_name", "processed"];

    public function scopeSearch($query, $search)
    {
        return $query->where('filename', 'LIKE', "%$search%")
            ->orWhere('original_name', 'LIKE', "%$search%")
            ->get();
    }
}
