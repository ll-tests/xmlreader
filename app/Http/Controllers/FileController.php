<?php

namespace App\Http\Controllers;

use App\File;
use Exception;
use Carbon\Carbon;
use App\Services\Utils;
use Illuminate\Http\Request;
use App\Services\PersonService;
use App\Services\ShipOrderService;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{

    protected $personService;
    protected $orderService;

    public function __construct(PersonService $service, ShipOrderService $order)
    {
        $this->personService = $service;
        $this->orderService = $order;
    }

    public function index()
    {
        return view('pages.files.index');
    }

    function list(Request $request) {
        if (isset($request->search) && !empty($request->search)) {
            $data = Upload::search($request->busca);
        } else {
            $data = File::all();
        }

        return view("pages.files.list", compact('data'));
    }

    public function store(Request $request)
    {
        $image = $request->file('file');
        $oldName = $this->getFileName($image);
        Log::info($oldName);
        $newName = Carbon::now()->format("Ymdhis") . "_" . $oldName;

        $image->move($this->getUploadPath(), $newName);

        $imageUpload = new File();
        $imageUpload->filename = $newName;
        $imageUpload->original_name = $oldName;
        if (!$imageUpload->save()) {
            $this->removePhysicalFile($this->getFilePath($image));
        }

        return response()->json(['success' => $newName]);
    }

    public function process()
    {
        $data = File::where("processed", false)->get();
        return view("pages.files.process", compact('data'));
    }

    public function postProcess(File $file)
    {
        $erros = [];
        if (isset($file->id)) {
            $result = $this->processFile($file);
            if ($result !== true) {
                Utils::setMessage("<strong>Oops!</strong> Error while trying to process file " . $file->filename, "danger");
                return redirect()->route("files.process");
            }
        } else {
            $files = File::where("processed", false)->get();
            foreach ($files as $file) {
                $result = $this->processFile($file);
                if ($result !== true) {
                    array_push($erros, $file->filename . " - " . $result);
                }
            }

            if (count($erros) > 0) {
                $strFiles = implode("<br>", $erros);
                Utils::setMessage("<strong>Oops!</strong> Error while trying to process bellow files:<br>" . $strFiles, "danger");
                return redirect()->route("files.process");
            }
        }

        Utils::setMessage("<strong>Yayyy!</strong> File(s) processed successfully!");
        return redirect()->route("files.process");
    }

    private function processFile($file)
    {
        try {
            $content = file_get_contents($this->getFilePath($file, false));
            $xml = simplexml_load_string($content);
        } catch (Exception $e) {
            Log::error("Erro while reading file " . $file->filename);
            Log::error($e->getMessage());
            return $e->getMessage();
        }

        $processed = false; 

        if (isset($xml->person)) $processed = $this->processPeople($xml);
        if (isset($xml->shiporder)) $processed = $this->processShipOrder($xml);

        if ($processed) {
            $file->processed = true;
            $file->save();
            return true;
        }

        return "File format is not valid";
    }

    private function processPeople($data)
    {
        $valid = true;
        foreach ($data as $person) {
            if (!$this->personService->insertPerson($person))
                $valid = false;
        }

        return $valid;
    }

    private function processShipOrder($data)
    {
        $valid = true;
        foreach ($data as $order) {
            if (!$this->orderService->insertShipOrder($order))
                $valid = false;
        }

        return $valid;
    }

    // deletes physical and logical file
    public function delete(File $file)
    {
        if (!empty($file)) {
            $item = $this->getFilePath($file, false);
            if (!file_exists($item)) {
                $file->delete();
            } else {
                if ($this->removePhysicalFile($item)) {
                    $file->delete();
                }
            }
        }
        Utils::setMessage("<strong>Well done!</strong> File delete successfully:<br>");
        return redirect()->route("files.list");
    }

    // deletes de physical file
    private function removePhysicalFile($file)
    {
        return unlink($file);
    }

    // returns de designated directory for uploads
    private function getUploadPath()
    {
        return storage_path('uploads');
    }

    // Returns de name of de logical or physical file
    private function getFileName($source, $isFile = true)
    {
        if ($isFile) {
            return $source->getClientOriginalName();
        }

        return $source->filename;

    }

    // returns the full path for the logical or physical file
    private function getFilePath($file, $isFile = true)
    {
        return $this->getUploadPath() . DIRECTORY_SEPARATOR . $this->getFileName($file, $isFile);
    }
}
