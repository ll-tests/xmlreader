<?php

namespace App\Http\Controllers;

use App\Services\PersonService;
use Symfony\Component\HttpFoundation\Request;

class PersonController extends Controller
{

    protected $service;

    public function __construct(PersonService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $data = $this->service->getList();
        return view('pages.person.index', compact("data"));
    }

    public function getApiList(Request $request)
    {
        return json_encode($this->service->getList($request));
    }
}
