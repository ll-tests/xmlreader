<?php

namespace App\Http\Controllers;

use App\Services\ShipOrderService;
use Symfony\Component\HttpFoundation\Request;

class ShipOrderController extends Controller
{

    protected $service;

    public function __construct(ShipOrderService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $data = $this->service->getList();
        return view('pages.shiporder.index', compact("data"));
    }

    public function getApiList(Request $request)
    {
        return json_encode($this->service->getList($request));
    }
}
