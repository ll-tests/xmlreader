<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = "person";
    protected $fillable = ["name"];

    public function phones()
    {
        return $this->hasMany("App\PersonPhone", "prs_id", "prs_id")->select("pph_id","prs_id","phone");
    }

    public function orders()
    {
        return $this->belongsToMany("App\ShipOrder", "sho_id");
    }
}
