<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonPhone extends Model
{
    protected $table = "person_phone";
    protected $fillable = ["prs_id", "phone"];

    public function person()
    {
        return $this->belongsTo("App\Person", "prs_id");
    }
}
