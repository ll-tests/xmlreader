<?

namespace App\Services;

use App\Person;
use App\PersonPhone;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PersonService
{

    public function getList($request = null)
    {
        $persons = Person::with(["phones"])->select("prs_id", "name", "created_at");
        if (isset($request->id) && $request->id != null ) {
            $persons->where("prs_id", $request->id);
        }
        return $persons->get();

    }

    public function insertPerson($person)
    {

        DB::beginTransaction();

        try {

            $model = Person::firstOrNew(["prs_id" => $person->personid]);
            $model->prs_id = $person->personid;
            $model->name = $person->personname;

            if (!$model->save()) {
                DB::rollback();
                return false;
            }

            if (count($person->phones->phone) > 0) {
                foreach ($person->phones->phone as $phone) {
                    $this->insertPhone($phone->__toString(), $model->prs_id);
                }
            } else {
                $this->insertPhone($person->phones->phone->__toString(), $model->prs_id);
            }

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Log::error("Error while inserting person");
            Log::error($e);
            return false;
        }
    }

    public function insertPhone($phone, $prs_id)
    {
        try {
            $model = PersonPhone::firstOrNew(["phone" => $phone, "prs_id" => $prs_id]);
            $model->phone = $phone;
            $model->prs_id = $prs_id;
            $model->save();
        } catch (Exception $e) {
            Log::error($e);
        }
    }

}
