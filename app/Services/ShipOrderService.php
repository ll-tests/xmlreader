<?

namespace App\Services;

use Exception;
use App\ShipOrder;
use App\ShipOrder_Item;
use App\ShipOrder_ShipTo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Request;

class ShipOrderService
{

    public function getList($request = null)
    {
        $orders =  ShipOrder::with("items", "person", "shipto")->select("sho_id", "prs_id", "created_at");
        if (isset($request) && $request->id != null) {
            $orders->wherE("sho_id", $request->id);
        }
        return $orders->get();
        

    }

    public function insertShipOrder($order)
    {

        DB::beginTransaction();

        try {

            $model = ShipOrder::firstOrNew(["sho_id" => $order->orderid]);
            $model->sho_id = $order->orderid;
            $model->prs_id = $order->orderperson;

            if (!$model->save()) {
                DB::rollback();
                return false;
            }

            $this->insertShipTo($order->shipto, $model->sho_id);
            $this->insertItem($order->items, $model->sho_id);

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            Log::error("Error while inserting ShipOrder");
            Log::error($e);
            return false;
        }
    }

    private function insertShipTo($shipTo, $order)
    {
        $model = ShipOrder_ShipTo::firstOrNew(["sho_id" => $order->__toString(), "name" => $shipTo->name->__toString()]);
        $model->sho_id = $order->__toString();
        $model->name = $shipTo->name->__toString();
        $model->address = $shipTo->address->__toString();
        $model->city = $shipTo->city->__toString();
        $model->country = $shipTo->country->__toString();
        $model->save();
    }

    private function insertItem($items, $order)
    {
        foreach ($items->item as $item) {
            
            // $item = $item->item;
            $model = ShipOrder_Item::firstOrNew(["sho_id" => $order->__toString(), "title" => $item->title->__toString()]);
            $model->note = $item->note->__toString();
            $model->qty = intval($item->quantity->__toString());
            $model->price = doubleval($item->price->__toString());
            $model->save();
        }
        
    }
}
