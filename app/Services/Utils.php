<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;

Class Utils {

    public static function setMessage($message, $type = "success")
    {
        Session::flash("dispMessageType", $type);
        Session::flash("dispMessage", $message);
    }

}