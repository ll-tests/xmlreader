<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipOrder extends Model
{
    protected $table = "shiporder";
    protected $fillable = ["sho_id", "prs_id"];

    public function items()
    {
        return $this->hasMany("App\ShipOrder_Item", "sho_id", "sho_id")->select("sho_id","title", "note", "price", "qty");
    }

    public function person()
    {
        return $this->hasOne("App\Person", "prs_id", "prs_id")->select("name","prs_id");
    }

    public function shipto()
    {
        return $this->hasOne("App\ShipOrder_ShipTo", "sho_id", "sho_id")->select("sho_id","name", "address", "city", "country");
    }
}
