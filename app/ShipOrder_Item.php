<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipOrder_Item extends Model
{
    protected $table = "shiporder_item";
    protected $fillable = ["title", "note", "qty", "price", "sho_id"];

    public function order()
    {
        return $this->belongsTo("App\ShipOrder", "sho_id");
    }
}
