<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipOrder_ShipTo extends Model
{
    protected $table = "shiporder_shipto";
    protected $fillable = ["sho_id","name","address","city","country"];

    public function order()
    {
        return $this->belongsTo("App\ShipOrder", "sho_id");
    }

}
