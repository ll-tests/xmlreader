<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shiporder_item', function (Blueprint $table) {
            $table->increments('soi_id');
            $table->integer("sho_id");
            $table->string('title')->nullable();
            $table->text('note')->nullable();
            $table->integer('qty')->nullable();
            $table->double('price', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shiporder_item');
    }
}
