## About S2IT XML Reader

S2IT XML Reader is a web application developed for S2IT during it's recruiting process.
It was based on 4 mandatory  and 4 bonus assignments.

I was able to complete 4 mandatory and 1 bonus assignments listed below: 

**Mandatory**
- [Symfony2, doctrine, composer, mysql.]
- [An index page to upload the XML with a button to process it.]
- [Rest APIs for the XML data, only the GET methods are needed.]
- [README.md with the instructions to install the application.]

**Bonus**
- [Drag and drop upload component.]


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- MySQL Database
- GIT


## Instalation

You can install the system with git by running the following command:

    git clone https://gitlab.com/luizlahr/xmlreader.git

After the clonning process is finished you need to install system's dependencys.
First access the folder where the repository was clonned by running the following command

    composer install

## Set Up

Rename the example.env file to .env and configure the below keys for databaseconnection.
A script to create the database is present by the end of this instructions.

    DB_HOST=
    DB_PORT=
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=

After configuring the database, execute the following command on the system's root folder to create the tables

    php artisan migrate

## Runing

For testing matters you can start the system by running the following command on system's root folder

    php artisan serve

Then you can access it on the http://localhost:8000

## API

Use the URL's below to retrieve information uploaded by XML files.
You can get all the information by the first two URL's, or get specific People or ShipOrder by replacing the **{id}** by the desired ID.

    localhost:8000/api/shiporders
    localhost:8000/api/people
    localhost:8000/api/shiporders/{id}
    localhost:8000/api/people/{id}



## Scripts

    CREATE SCHEMA s2it_xml DEFAULT CHARACTER SET utf8 ;