function loading(loading) {
    loading = loading == undefined ? true : false;
    if (loading) {
        $('#loader-drop').removeClass('loaded');
    } else {
        $('#loader-drop').addClass('loaded');
    }
}