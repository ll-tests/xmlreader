<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ env("APP_NAME") }}</title>

    {{ Html::script('/vendor/jquery/jquery.min.js') }}
    {{ Html::script('/vendor/bootstrap/js/bootstrap.min.js') }}


    {{ Html::style('/vendor/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('https://fonts.googleapis.com/icon?family=Material+Icons') }}

    @stack('styles')

    {{ Html::style('/css/app.css') }}

</head>

<body>

    <div id="loader-drop" class="loaded">
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
    </div>

    <div class="container">
        @include('layout.partials.header')
        <h1 class="page-title text-center">@yield('page-title')</h1>
        @if(Session::has("dispMessage"))
        <div class="alert alert-{{ Session::get('dispMessageType') }} alert-dismissible fade show" role="alert">
            {!! Session::get('dispMessage') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="row">
            <div class="col">
                @yield("body")
            </div>
        </div>
    </div>

    {{ Html::script('js/all.js') }}

    @stack('scripts')

</body>

</html>
