<nav class="navbar navbar-dark bg-primary navbar-expand-md">

    {{-- LOGO --}}
    <a class="navbar-brand" href="#">
        {{ env("APP_NAME") }}
    </a>

    
    {{--  TOGGLE  --}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


    {{--  MENU ITEMS  --}}
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            @if (\Request::is('companies'))  
  Companies menu
@endif
            <li class="nav-item {{ \Request::is('files') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('files.index') }}">Uploads</span></a>
            </li>
            <li class="nav-item {{ \Request::is('files/process') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('files.process') }}">Process</a>
            </li>
            <li class="nav-item {{ \Request::is('person') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('person.index') }}">People</a>
            </li>
            <li class="nav-item {{ \Request::is('shiporder') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('shiporder.index') }}">ShipOrders</a>
            </li>
            <li class="nav-item {{ \Request::is('files/list') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('files.list') }}">Files</a>
            </li>
        </ul>
    </div>

</nav>
