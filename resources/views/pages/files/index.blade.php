@extends('layout.base')

@section('page-title', 'XML Uploads')

@section("body")

<div class="row">
    <div class="col-12">
        <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data" class="dropzone" id="dropzone">
            @csrf
        </form>
    </div>
</div>

<div class="row">
    <div class="col-12" style="margin-top: 20px; font-size: 12px; font-style: italic; color: #666;">
        <ul>
            <li>
                ShipOrders with existing orderid will not be inserted, the existing data will be updated.
            </li>
            <li>
                ShipOrder Items will not be duplicated within same orderid, the existing data will be updated.
            </li>
            <li>
                People with existing personid will not be inserted, the existing data will be updated.
            </li>
            <li>
                Phone numbers will not be duplicated within same person, the existing data will be updated.
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-12" style="margin-top: 20px;">
        {!! Form::open(["route"=>"files.process", "method"=>"post", "id"=>"frmProccess", "class"=>"text-center"]) !!}
        <button class="btn btn-primary">
            <span style="text-transform: uppercase;">Process All Files</span>
        </button>
        {!! Form::close() !!}
    </div>
</div>
@endsection

@push('styles')
{{ Html::style('vendor/dropzone/dropzone.css') }}
@endpush


@push('scripts')
{{ Html::script('vendor/dropzone/dropzone.js') }}

<script>
    Dropzone.options.dropzone = {
        maxFilesize: 12,
        acceptedFiles: ".xml",
        addRemoveLinks: true,
        timeout: 5000,
        dictDefaultMessage: "Drop your files here (.xml only) ",
        dictInvalidFileType: "Invalid file type!",
        success: function (file, response) {
            console.log(file.name);
        },
        error: function (file, response) {
            alert(response);
            console.log(response);
        },
        init: function () {
            this.on("complete", function (file) {
                {
                    {
                        --removeArq(this, file);
                        --
                    }
                }
                console.log("done");
            })
        }
    };

    function removeArq(element, file) {
        setTimeout(function () {
            element.removeFile(file);
        }, 2000);
    }

</script>
@endpush
