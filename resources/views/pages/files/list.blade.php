@extends('layout.base')

@section('page-title', 'Files')

@section("body")

<table class="table table-hover">
    <thead class="bg-info">
        <tr>
            <th scope="col">File</th>
            <th scope="col">Physical File</th>
            <th scope="col">Uploaded</th>
            <th scope="col">Processed</th>
            <th scope="col">Remove</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($data) && count($data) > 0)
        @foreach($data as $file)
        <tr>
            <td>{{ $file->filename }}</td>
            <td>{{ $file->original_name }}</td>
            <td>{{ $file->created_at }}</td>
            <td>
                @if($file->processed)
                    <div class="material-icons text-success">check</div>
                @else
                    <div class="material-icons text-danger">clear</div>
                @endif
            </td>
            <td>
                {!! Form::open(["route"=>["files.delete", $file->id]]) !!}
                <button type="submit" class="btn btn-link" style="padding: 0px;">
                    <div class="material-icons text-black-50">delete</div>
                </button>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="5" class="text-center text-danger">
                No data found!
            </td>
        </tr>
        @endif
    </tbody>
</table>

@endsection
