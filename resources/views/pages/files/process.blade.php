@extends('layout.base')

@section('page-title', 'File Processing')

@section("body")

<table class="table table-hover">
    <thead class="bg-info">
        @if(isset($data) && count($data) > 0)
        <tr>
            <th colspan="4">
                {!! Form::open(["route"=>"files.process", "method"=>"post", "id"=>"frmProccess", "class"=>"text-center"]) !!}
                    <button class="btn btn-light">
                        <span style="text-transform: uppercase;">Process All Files</span>
                    </button>
                {!! Form::close() !!}
            </th>
        </tr>
        @endif
        <tr>
            <th scope="col">File</th>
            <th scope="col">Physical File</th>
            <th scope="col">Uploaded</th>
            <th scope="col">Process Single File</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($data) && count($data) > 0)
        @foreach($data as $file)
        <tr>
            <td>{{ $file->filename }}</td>
            <td>{{ $file->original_name }}</td>
            <td>{{ $file->created_at }}</td>
            <td>
                {!! Form::open(["route"=>["files.process", $file->id]]) !!}
                <button type="submit" class="btn btn-link" style="padding: 0px;" title="Process">
                    <div class="material-icons text-primary">play_circle_outline</div>
                </button>
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="4" class="text-center text-danger">
                No files to process!            
            </td>
        </tr>
        @endif
    </tbody>
</table>

@endsection
