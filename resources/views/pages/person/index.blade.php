@extends('layout.base')

@section('page-title', 'People')

@section("body")

<table class="table table-hover">
    <thead class="bg-info">
        <tr>
            <th scope="col">Person ID</th>
            <th scope="col">Name</th>
            <th scope="col">Phones</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($data) && count($data) > 0)
        @foreach($data as $person)
        <tr>
            <td>{{ $person->prs_id }}</td>
            <td>{{ $person->name }}</td>
            <td>
                <h5>
                    @foreach($person->phones as $phone)
                    <span class="badge badge-secondary badge-pill badge-lg">{{ $phone->phone }}</span>
                    @endforeach
                </h5>
            </td>
        </tr>

        @endforeach
        @else
        <tr>
            <td colspan="5" class="text-center text-danger">
                No data found!
            </td>
        </tr>
        @endif
    </tbody>
</table>

@endsection
