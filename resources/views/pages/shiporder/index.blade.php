@extends('layout.base')

@section('page-title', 'Ship Orders')

@section("body")

<style>
    .btn.collapsed .down {
        display: inline-block;
    }

    .btn.collapsed .up {
        display: none;
    }

    .btn .up {
        display: inline-block;
    }

    .btn:not(.collapsed) .down {
        display: none;
    }
</style>
<table class="table">
    <thead class="bg-info">
        <tr>
            <th scope="col">Order #</th>
            <th scope="col">Person ID / Name</th>
            <th scope="col">Ship To</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @if(isset($data) && count($data) > 0)
            @foreach($data as $order)
                <tr>
                    <td style="border-top: 0;">{{ $order->sho_id }}</td>
                    <td style="border-top: 0;">{{ isset($order->person) ? $order->prs_id ." - ".$order->person->name : $order->prs_id }}</td>
                    <td style="border-top: 0;">
                        @if(isset($order->shipto))
                            {{ $order->shipto->name }} ({{ $order->shipto->address }}, {{ $order->shipto->city }} - {{
                            $order->shipto->country }})
                        @endif
                    </td>
                    <td style="border-top: 0;">
                        @if(isset($order->items))
                            <a class="btn btn-outline-primary collapsed" data-toggle="collapse" href="#items-{{ $order->sho_id }}" role="button" aria-expanded="false"
                                aria-controls="collapseExample" style="position: relative; padding-right: 30px;">
                                ITEMS
                                <div class="material-icons down" style="bottom: 5px; position: absolute;">
                                    arrow_drop_down
                                </div>
                                <div class="material-icons up" style="bottom: 5px; position: absolute;">
                                    arrow_drop_up
                                </div>
                            </a>
                        @endif
                    </td>
                </tr>

                @if(isset($order->items) && count($order->items) > 0)
                    <tr>
                        <td colspan="4">
                            <div class="collapse" id="items-{{ $order->sho_id }}">
                            <table class="table table-borderless table-dark">
                                <thead style="background-color: rgba(0,0,0,0.3)">
                                    <tr>
                                        <th>Item</th>
                                        <th>Note</th>
                                        <th>Qty</th>
                                        <th>Price (unit)</th>
                                        <th>Item Total</th>
                                    </tr>
                                </thead>
                                
                                @php
                                    $shipTotal = 0;
                                @endphp
                                
                                @foreach($order->items as $item)
                                
                                    <tbody style="background-color: rgba(0,0,0,0.2); color: #444;">
                                        <tr>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->note }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ round($item->price * $item->qty, 2) }}</td>
                                            @php
                                                $shipTotal += round($item->price * $item->qty, 2)
                                            @endphp
                                        </tr>
                                    </tbody>
                                
                                @endforeach
                                
                                <tfoot style="background-color: rgba(0,0,0,0.3);">
                                    <tr>
                                        <td colspan="4" class="text-right" style="font-weight: bold; padding: 0 20px; text-transform: uppercase;">Total:</td>
                                        <td style="font-weight: bold; padding: 0 20px; text-transform: uppercase;">{{ $shipTotal }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                            </div>
                        </td>
                    </tr>
                @endif

            @endforeach
        @else
            <tr>
                <td colspan="5" class="text-center text-danger">
                    No data found!
                </td>
            </tr>
        @endif
    </tbody>
</table>

@endsection
