<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/people/{id?}", "PersonController@getApiList");
Route::get("/shiporders/{id?}", "ShipOrderController@getApiList");



// Route::middleware('auth:api')->group(function() {
//     Route::get("/people", function(){
//         return "people";
//     });
// });


