<?

Route::prefix('files')->name('files.')->group(function(){

    Route::get('/', 'FileController@index')->name('index');
    Route::get('/list', 'FileController@list')->name('list');
    Route::post('/store', 'FileController@store')->name('store');
    Route::get('/process', 'FileController@process')->name('process');
    Route::post('/process/{file?}', 'FileController@postProcess')->name('process');
    Route::post('/{file}/delete', 'FileController@delete')->name('delete');
    Route::get('/{file}/download', 'FileController@download')->name('download');

});